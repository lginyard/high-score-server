import express from "express"
import scores from "./scores.json"

const app = express()
    //middleware//
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

//get request for whatever info we feed through this functionality//
app.get("/scores", (req, res) => {
    //determining which is higher s or y// 
    res.body = scores.sort((x, y) => y.score - x.score)
    let sortedScores = res.body.slice(0, 3)
    let newArray = JSON.stringify(sortedScores)

    res.status(200).send(newArray)
})

app.get("*", (req, res) => {
    res.status(404).send("Error Page Not Found")
})

//prompts app to post whatever we feed into here to our scores array//
app.post("/scores", (req, res) => {
    const player = req.body
    scores.push(player)
    res.status(201).send("Ok")
})






app.listen(2000, () => {
    console.log("Express server is running on port 2000")
})